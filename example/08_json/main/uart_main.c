
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "esp_system.h"
#include "esp_log.h"
#include "esp_err.h"

#include "esp_spi_flash.h"
#include "driver/gpio.h"

#include "driver/uart.h"
#include "cJSON.h"

#include "my_uart.h"
static const char *TAG = "uart demo";

const char *json_buf="{\"id\":6572,\"uuid\":\"57b44bef-84c2-417e-9f57-6789a3ee443e\",\"hitokoto\":\"人民是健忘的，但他们总有其他方式去记住。\",\"type\":\"c\",\"from\":\"«狂欢节»，TNO启示录事件\",\"from_who\":\"The New Order:Last Days of Europe\",\"creator\":\"Forevercontinent\",\"creator_uid\":7134,\"reviewer\":6844,\"commit_from\":\"web\",\"created_at\":\"1599387781\",\"length\":20}";

char keys[][20] = {"id","uuid","hitokoto","type","from","from_who","creator","creator_uid","reviewer","commit_from","created_at","length"};

#define GPIO_PIN_LED 2
#define GPIO_OUTPUT_PIN (1UL << GPIO_PIN_LED)

void Gpio_Init(void)
{
    gpio_config_t IO_config;

    IO_config.pin_bit_mask = GPIO_OUTPUT_PIN;
    IO_config.mode = GPIO_MODE_OUTPUT;
    IO_config.pull_up_en = GPIO_PULLUP_DISABLE;
    IO_config.pull_down_en = GPIO_PULLDOWN_DISABLE;
    IO_config.intr_type = GPIO_INTR_DISABLE;

    gpio_config(&IO_config);
}

void get_system_info(void)
{
    ESP_LOGI(TAG,"\nhello world\n");
    //获取IDF版本
    ESP_LOGI(TAG,"SDK version:%s\n", esp_get_idf_version());

    //获取芯片可用内存
    ESP_LOGI(TAG,"esp_get_free_heap_size : %d  \n", esp_get_free_heap_size());
    //获取从未使用过的最小内存
    ESP_LOGI(TAG,"esp_get_minimum_free_heap_size : %d  \n", esp_get_minimum_free_heap_size());
    //获取芯片的内存分布，返回值具体见结构体 flash_size_map
    ESP_LOGI(TAG,"system_get_flash_size_map(): %d \n", system_get_flash_size_map());
    //获取mac地址（station模式）
    uint8_t mac[6];
    esp_read_mac(mac, ESP_MAC_WIFI_STA);
    ESP_LOGI(TAG,"esp_read_mac(): %02x:%02x:%02x:%02x:%02x:%02x \n", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);

    /* Print chip information */
    esp_chip_info_t chip_info;
    esp_chip_info(&chip_info);
    ESP_LOGI(TAG,"This is ESP8266 chip with %d CPU cores, WiFi, ",
        chip_info.cores);
    ESP_LOGI(TAG,"silicon revision %d, ", chip_info.revision);
    ESP_LOGI(TAG,"%dMB %s flash\n", spi_flash_get_chip_size() / (1024 * 1024),
        (chip_info.features & CHIP_FEATURE_EMB_FLASH) ? "embedded" : "external");
}

void uart_recv_task(void *argv)
{
    uint8_t *buf = calloc(1024,1);
    int len = 0;
    while (1)
    {
        my_uart_recv_data(UART_NUM_0,buf,&len);
        if(len > 0){
            buf[len] = 0;
            ESP_LOGW(TAG,"RX:%s",buf);
        }
    }
}

void uart_send_task(void *argv)
{
    while(1) {
        my_uart_send_data(UART_NUM_0,(uint8_t *)"hello world\r\n",strlen("hello world\r\n"));
        vTaskDelay(500/ portTICK_RATE_MS);
    }
}


void cJson_Read(void)
{
    cJSON *root;
    cJSON *data;
    int data_num = 0;
    int i;
    root = cJSON_Parse(json_buf);
    data_num = sizeof(keys)/20;
    ESP_LOGI(TAG,"json data num:%d\n",data_num);
    for (i = 0; i<data_num; i++)
    {
        data = cJSON_GetObjectItem(root,keys[i]);

        if(data->valuestring == NULL)
        {
            ESP_LOGE(TAG,"%s:NULL",data->string);
        }
        else
        {
            ESP_LOGW(TAG,"%s:%s",data->string,data->valuestring);
        }
    }
}


void app_main()
{   
    ESP_LOGE(TAG,"ESP_LOGE");
    ESP_LOGW(TAG,"ESP_LOGW");
    ESP_LOGI(TAG,"ESP_LOGI");
    ESP_LOGD(TAG,"ESP_LOGD");
    ESP_LOGV(TAG,"ESP_LOGV");

    ESP_LOGI(TAG,"start Gpio_Init");
    Gpio_Init();
    get_system_info();
    
    ESP_LOGI(TAG,"start run");
    cJson_Read();
}
