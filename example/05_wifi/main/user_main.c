#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "esp_system.h"
#include "esp_log.h"
#include "esp_err.h"

#include "esp_spi_flash.h"
#include "driver/gpio.h"

#include "my_wifi.h"

static const char *TAG = "wifi demo";

#define GPIO_PIN_LED 2
#define GPIO_OUTPUT_PIN (1UL << GPIO_PIN_LED)

void Gpio_Init(void)
{
    gpio_config_t IO_config;

    IO_config.pin_bit_mask = GPIO_OUTPUT_PIN;
    IO_config.mode = GPIO_MODE_OUTPUT;
    IO_config.pull_up_en = GPIO_PULLUP_ENABLE;
    IO_config.pull_down_en = GPIO_PULLDOWN_DISABLE;
    IO_config.intr_type = GPIO_INTR_DISABLE;

    gpio_config(&IO_config);
}

void app_main()
{   
    ESP_LOGE(TAG,"ESP_LOGE");
    ESP_LOGW(TAG,"ESP_LOGW");
    ESP_LOGI(TAG,"ESP_LOGI");
    ESP_LOGD(TAG,"ESP_LOGD");
    ESP_LOGV(TAG,"ESP_LOGV");

    ESP_ERROR_CHECK(nvs_flash_init());

    ESP_LOGI(TAG,"start Gpio_Init");
    Gpio_Init();

    my_wifi_t wifi_info = {
        .ssid = "HONOR-0415PD",
        .password = "pf0105602"
    };

    ESP_LOGI(TAG, "ESP_WIFI_MODE_STA");
    if (wifi_init_sta(&wifi_info)){
        ESP_LOGE(TAG, "CONNECT TO AP FAIL");
    }

    ESP_LOGI(TAG, "Run");
}
