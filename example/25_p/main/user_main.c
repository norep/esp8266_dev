#include "user_main.h"

static const char *TAG = "global";

#if SUPPORT_FIRMWARE_OTA
static const char *ota_version_url = "http://norep.gitee.io/ota/ota_test/version";
static const char *ota_image_url = "http://norep.gitee.io/ota/ota_test/ota.bin";
#endif

static int user_get_cb(httpd_req_t *req)
{
	httpd_resp_send(req, index_html_start, index_html_end - index_html_start);
	return 0;
}

static int user_post_cb(httpd_req_t *req, char *content, int len)
{
	system_status_t *sys_sta = get_sys_sta();
	char ssid[32];
	char passwd[32];
	int ret = httpd_query_key_value(content,"ssid",ssid,sizeof(ssid));
	if(ret == ESP_OK) {
		printf("ssid = %s\n",ssid);
	}
	else {
		printf("error = %d\n",ret);
	}

	ret = httpd_query_key_value(content,"password",passwd,sizeof(passwd));
	if(ret == ESP_OK) {
		printf("password = %s\n",passwd);
	}
	else {
		printf("error = %d\n",ret);
	}

	if (strlen(ssid) && strlen(passwd)) {
		my_nvs_wifi_info_set(ssid, passwd);
	}
	httpd_resp_send(req, save_html_start, save_html_end - save_html_start);
	sys_sta->reboot_flag = 1;
	return 0;
}

static int shortdown(void)
{
	ESP_LOGW(TAG, "===== SHOART DOWN =====");
	return 0;
}
	
static int longdown(void)
{
	ESP_LOGW(TAG, "===== LONG DOWN =====");
	return 0;
}

static void WIFI_Connect_cb(char *ip)
{
	system_status_t *sys_sta = get_sys_sta();
	sys_sta->network_sta = 1;
	ESP_LOGW(TAG, "wifi connect : %s", ip);
}

static void WIFI_Disconnect_cb(void)
{
	system_status_t *sys_sta = get_sys_sta();
	sys_sta->network_sta = 0;
	ESP_LOGW(TAG, "wifi disconnect");
}

static void WIFI_AP_STAConnect_cb(void)
{
	ESP_LOGW(TAG, "WIFI_AP_STAConnect_cb");

	my_http_cb_t user_cb;
	user_cb.get_cb = user_get_cb;
	user_cb.post_cb = user_post_cb;
	my_http_server_set_callback(user_cb);
	ESP_LOGW(TAG, "Starting webserver");
	my_start_webserver();

}

static void WIFI_AP_STADisconnect_cb(void)
{
	ESP_LOGW(TAG, "WIFI_AP_STADisconnect_cb");
}

static void Networl_init(void)
{
	my_wifi_t wifi_info = {};
	wifi_mode_t wifi_init_mode = WIFI_MODE_AP;

	if (my_nvs_wifi_info_get(&wifi_info)) {

		memset(&wifi_info, 0, sizeof(wifi_info));
		memcpy(wifi_info.ssid, DEFAULT_AP_MODE_SSID, strlen(DEFAULT_AP_MODE_SSID));

		if (strlen(DEFAULT_AP_MODE_PASSWORD) > 0) {
			memmove(wifi_info.password, DEFAULT_AP_MODE_PASSWORD, strlen(DEFAULT_AP_MODE_PASSWORD));
		}

		wifi_info.max_connection = 1;

		wifi_info.ap_staconnect_cb = WIFI_AP_STAConnect_cb;
		wifi_info.ap_stadisconnect_cb = WIFI_AP_STADisconnect_cb;

		wifi_init_mode = WIFI_MODE_AP;
	} else
	 {
		sprintf((char*)wifi_info.ssid, "HONOR-0415PD");
		sprintf((char*)wifi_info.password, "pf0105602");

		wifi_info.connect_cb = WIFI_Connect_cb;
		wifi_info.disconnect_cb = WIFI_Disconnect_cb;

		wifi_init_mode = WIFI_MODE_STA;
	}
	my_wifi_init(&wifi_info, wifi_init_mode);
}

void app_main()
{
	system_status_t *sys_sta = get_sys_sta();
	const esp_app_desc_t *app_desc = esp_ota_get_app_description();
	sys_sta->version = (char *)app_desc->version;
	ESP_LOGI(TAG, "app_version:%s", sys_sta->version); 

#if SUPPORT_FIRMWARE_OTA
	sys_sta->ota_version_path = ota_version_url;
	sys_sta->ota_image_path = ota_image_url;
#endif

	system_init();

	printf("free_heap_size: %d KB\n", esp_get_free_heap_size() / 1024);
	printf("minimum_free_heap_size: %d KB\n", esp_get_minimum_free_heap_size() / 1024);

	set_key_shortDown_callback(200, shortdown);
	set_key_longDown_callback(2000, longdown);

	ESP_LOGI(TAG, "start connect wifi...");
	Networl_init();
	ESP_LOGI(TAG, "Run");

    esp_wifi_set_ps(WIFI_PS_MAX_MODEM);
    esp_pm_config_esp8266_t pm_config = {
            .light_sleep_enable = true
    };
    ESP_ERROR_CHECK(esp_pm_configure(&pm_config));
}
