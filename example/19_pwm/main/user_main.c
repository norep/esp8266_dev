#include "user_main.h"

static const char *TAG = "19_pwm";

static const char *ota_version_url = "http://norep.gitee.io/ota/ota_test/version";
static const char *ota_image_url = "http://norep.gitee.io/ota/ota_test/ota.bin";

int user_get_cb(httpd_req_t *req)
{
    httpd_resp_send(req, index_html_start, index_html_end - index_html_start);
    return 0;
}

int user_post_cb(httpd_req_t *req, char *content, int len)
{
    system_status_t *sys_sta = get_sys_sta();
    char ssid[32];
    char passwd[32];
    int ret = httpd_query_key_value(content,"ssid",ssid,sizeof(ssid));
    if(ret == ESP_OK) {
        printf("ssid = %s\n",ssid);
    }
    else {
        printf("error = %d\n",ret);
    }

    ret = httpd_query_key_value(content,"password",passwd,sizeof(passwd));
    if(ret == ESP_OK) {
        printf("password = %s\n",passwd);
    }
    else {
        printf("error = %d\n",ret);
    }

    if (strlen(ssid) && strlen(passwd)) {
        my_nvs_wifi_info_set(ssid, passwd);
    }
    httpd_resp_send(req, save_html_start, save_html_end - save_html_start);
    sys_sta->reboot_flag = 1;
    return 0;
}

int shortdown(void)
{
    ESP_LOGW(TAG, "===== SHOART DOWN =====");
    return 0;
}
    
int longdown(void)
{
    ESP_LOGW(TAG, "===== LONG DOWN =====");
    return 0;
}

void WIFI_Connect_cb(char *ip)
{
    system_status_t *sys_sta = get_sys_sta();
    sys_sta->network_sta = 1;
    ESP_LOGW(TAG, "wifi connect : %s", ip);
}

void WIFI_Disconnect_cb(void)
{
    system_status_t *sys_sta = get_sys_sta();
    sys_sta->network_sta = 0;
    ESP_LOGW(TAG, "wifi disconnect");
}

void WIFI_AP_STAConnect_cb(void)
{
    ESP_LOGW(TAG, "WIFI_AP_STAConnect_cb");

    my_http_cb_t user_cb;
    user_cb.get_cb = user_get_cb;
    user_cb.post_cb = user_post_cb;
    my_http_server_set_callback(user_cb);
    ESP_LOGW(TAG, "Starting webserver");
    my_start_webserver();

}

void WIFI_AP_STADisconnect_cb(void)
{
    ESP_LOGW(TAG, "WIFI_AP_STADisconnect_cb");
}

void Networl_init(void)
{
    my_wifi_t wifi_info = {};
    wifi_mode_t wifi_init_mode = WIFI_MODE_AP;

    if (my_nvs_wifi_info_get(&wifi_info)) {

        memset(&wifi_info, 0, sizeof(wifi_info));
        memcpy(wifi_info.ssid, DEFAULT_AP_MODE_SSID, strlen(DEFAULT_AP_MODE_SSID));

        if (strlen(DEFAULT_AP_MODE_PASSWORD) > 0) {
            memmove(wifi_info.password, DEFAULT_AP_MODE_PASSWORD, strlen(DEFAULT_AP_MODE_PASSWORD));
        }

        wifi_info.max_connection = 1;

        wifi_info.ap_staconnect_cb = WIFI_AP_STAConnect_cb;
        wifi_info.ap_stadisconnect_cb = WIFI_AP_STADisconnect_cb;

        wifi_init_mode = WIFI_MODE_AP;
    } else {
        wifi_info.connect_cb = WIFI_Connect_cb;
        wifi_info.disconnect_cb = WIFI_Disconnect_cb;

        wifi_init_mode = WIFI_MODE_STA;
    }
    my_wifi_init(&wifi_info, wifi_init_mode);
}

// PWM period 1000us(1Khz), same as depth
#define PWM_PERIOD    (20000)

const uint32_t pin_num[1] = {
    GPIO_NUM_2,
};

// duties table, real_duty = duties[x]/PERIOD
uint32_t duties[1] = {
    1500,
};

// phase table, delay = (phase[x]/360)*PERIOD
float phase[1] = {
    0,
};

int user_pwm_init(void)
{
    uint32_t duty = 0;
    system_status_t *sys_sta = get_sys_sta();
    pwm_init(PWM_PERIOD, duties, 1, pin_num);
    pwm_set_phases(phase);
    pwm_start();
    while (1)
    {
        for (duty = 500; duty < 2500; duty += 20)
        {
            pwm_set_duty(0, duty);
            pwm_start();
            vTaskDelay(100 / portTICK_RATE_MS);
        }
        vTaskDelay(500 / portTICK_RATE_MS);
        for (duty = 500; duty < 2500; duty += 20)
        {
            pwm_set_duty(0, 3000 - duty);
            pwm_start();
            vTaskDelay(100 / portTICK_RATE_MS);
        }
        vTaskDelay(500 / portTICK_RATE_MS);
    }
}

void app_main()
{
    system_status_t *sys_sta = get_sys_sta();
    const esp_app_desc_t *app_desc = esp_ota_get_app_description();
    sys_sta->version = (char *)app_desc->version;
    ESP_LOGI(TAG, "app_version:%s", sys_sta->version); 
    
    system_init();

    printf("free_heap_size: %d KB\n", esp_get_free_heap_size() / 1024);
    printf("minimum_free_heap_size: %d KB\n", esp_get_minimum_free_heap_size() / 1024);

    set_key_shortDown_callback(200, shortdown);
    set_key_longDown_callback(2000, longdown);

    sys_sta->ota_version_path = ota_version_url;
    // sys_sta->ota_image_path = ota_image_url;
    
    // ESP_LOGI(TAG, "start connect wifi...");
    // Networl_init();
    ESP_LOGI(TAG, "Run");

    user_pwm_init();
}
