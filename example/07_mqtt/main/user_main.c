#include "user_main.h"

#include "nvs_flash.h"
#include "nvs.h"

static const char *TAG = "global";

static const char *ota_version_url = "http://norep.gitee.io/ota/ota_test/version";
static const char *ota_image_url = "http://norep.gitee.io/ota/ota_test/ota.bin";

int user_get_cb(httpd_req_t *req)
{
	httpd_resp_send(req, index_html_start, index_html_end - index_html_start);
	return 0;
}

int user_post_cb(httpd_req_t *req, char *content, int len)
{
	system_status_t *sys_sta = get_sys_sta();
	char ssid[32];
	char passwd[32];
	int ret = httpd_query_key_value(content,"ssid",ssid,sizeof(ssid));
	if(ret == ESP_OK) {
		printf("ssid = %s\n",ssid);
	}
	else {
		printf("error = %d\n",ret);
	}

	ret = httpd_query_key_value(content,"password",passwd,sizeof(passwd));
	if(ret == ESP_OK) {
		printf("password = %s\n",passwd);
	}
	else {
		printf("error = %d\n",ret);
	}

	if (strlen(ssid) && strlen(passwd)) {
		my_nvs_wifi_info_set(ssid, passwd);
	}
	httpd_resp_send(req, save_html_start, save_html_end - save_html_start);
	sys_sta->reboot_flag = 1;
	return 0;
}

int shortdown(void)
{
	ESP_LOGW(TAG, "===== SHOART DOWN =====");
	return 0;
}
	
int longdown(void)
{
	ESP_LOGW(TAG, "===== LONG DOWN =====");
	return 0;
}

void WIFI_Connect_cb(char *ip)
{
	system_status_t *sys_sta = get_sys_sta();
	sys_sta->network_sta = 1;
	ESP_LOGW(TAG, "wifi connect : %s", ip);
}

void WIFI_Disconnect_cb(void)
{
	system_status_t *sys_sta = get_sys_sta();
	sys_sta->network_sta = 0;
	ESP_LOGW(TAG, "wifi disconnect");
}

void WIFI_AP_STAConnect_cb(void)
{
	ESP_LOGW(TAG, "WIFI_AP_STAConnect_cb");

	my_http_cb_t user_cb;
	user_cb.get_cb = user_get_cb;
	user_cb.post_cb = user_post_cb;
	my_http_server_set_callback(user_cb);
	ESP_LOGW(TAG, "Starting webserver");
	my_start_webserver();

}

void WIFI_AP_STADisconnect_cb(void)
{
	ESP_LOGW(TAG, "WIFI_AP_STADisconnect_cb");
}

void Networl_init(void)
{
	my_wifi_t wifi_info = {};
	wifi_mode_t wifi_init_mode = WIFI_MODE_AP;

	if (my_nvs_wifi_info_get(&wifi_info)) {

		memset(&wifi_info, 0, sizeof(wifi_info));
		memcpy(wifi_info.ssid, DEFAULT_AP_MODE_SSID, strlen(DEFAULT_AP_MODE_SSID));

		if (strlen(DEFAULT_AP_MODE_PASSWORD) > 0) {
			memmove(wifi_info.password, DEFAULT_AP_MODE_PASSWORD, strlen(DEFAULT_AP_MODE_PASSWORD));
		}

		wifi_info.max_connection = 1;

		wifi_info.ap_staconnect_cb = WIFI_AP_STAConnect_cb;
		wifi_info.ap_stadisconnect_cb = WIFI_AP_STADisconnect_cb;

		wifi_init_mode = WIFI_MODE_AP;
	} else {
		wifi_info.connect_cb = WIFI_Connect_cb;
		wifi_info.disconnect_cb = WIFI_Disconnect_cb;

		wifi_init_mode = WIFI_MODE_STA;
	}
	my_wifi_init(&wifi_info, wifi_init_mode);
}

void MY_MQTT_Connect_cb(void)
{
	my_mqtt_subscribe("/topic/ULYMGS/TH/get", 0);
}

void MY_MQTT_DISConnect_cb(void)
{
}

void MY_MQTT_RecData_cb(char *topic, int topic_len, char *data, int data_len)
{
	printf("topic: %.*s\t data:%.*s\n", topic_len, topic, data_len, data);
	my_mqtt_publish("/topic/ULYMGS/TH/set", data, data_len, 0, 0);
}

void init_my_mqtt()
{
	my_mqtt_init_info_t mqtt_init_cfg = {0};

	mqtt_init_cfg.uri = "mqtt://broker-cn.emqx.io";
	mqtt_init_cfg.connect_cb = MY_MQTT_Connect_cb;
	mqtt_init_cfg.disconnect_cb = MY_MQTT_DISConnect_cb;
	mqtt_init_cfg.received_cb = MY_MQTT_RecData_cb;

	my_mqtt_init(mqtt_init_cfg);
}

void app_main()
{
	system_status_t *sys_sta = get_sys_sta();
	const esp_app_desc_t *app_desc = esp_ota_get_app_description();
	sys_sta->version = (char *)app_desc->version;
	ESP_LOGI(TAG, "app_version:%s", sys_sta->version); 
	
	system_init();

	printf("free_heap_size: %d KB\n", esp_get_free_heap_size() / 1024);
	printf("minimum_free_heap_size: %d KB\n", esp_get_minimum_free_heap_size() / 1024);

	set_key_shortDown_callback(200, shortdown);
	set_key_longDown_callback(2000, longdown);

	sys_sta->ota_version_path = ota_version_url;
	// sys_sta->ota_image_path = ota_image_url;
	
	ESP_LOGI(TAG, "start connect wifi...");
	Networl_init();
	ESP_LOGI(TAG, "Run");

	while (sys_sta->network_sta != 1)
	{
		vTaskDelay(1000 / portTICK_PERIOD_MS);
	}
	init_my_mqtt();

	char buf[128];

	while (1)
	{
		while (sys_sta->network_sta)
		{
			memset(buf, 0, sizeof(buf));
			snprintf(buf, sizeof(buf), "{%d/%d/%d %02d:%02d:%02d}", 
									1900 + timeinfo.tm_year, timeinfo.tm_mon, timeinfo.tm_mday,
									timeinfo.tm_hour, timeinfo.tm_min, timeinfo.tm_sec);

			my_mqtt_publish("/topic/ULYMGS/TH/set", buf, 0, 0, 0);
			vTaskDelay(1000 / portTICK_PERIOD_MS);
		}
		vTaskDelay(1000 / portTICK_PERIOD_MS);
	}
}
