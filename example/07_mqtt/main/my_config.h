#ifndef __MY_CONFIG_H__
#define __MY_CONFIG_H__

#define DEFAULT_AP_MODE_SSID     "esp8266_ap"
#define DEFAULT_AP_MODE_PASSWORD ""

#define MY_HOST_NAME             "esp8266"

#define ENABLE_LED_FUNCTIONS     1

#if ENABLE_LED_FUNCTIONS
#define GPIO_PIN_LED             GPIO_NUM_2
#define GPIO_OUTPUT_PIN          (1ULL << GPIO_PIN_LED)
#define LED_WORK_UP              0
#define LED_WORK_DOWN            1
#endif

#define ENABLE_KEY_FUNCTIONS     1

#if ENABLE_KEY_FUNCTIONS
#define GPIO_PIN_KEY             GPIO_NUM_0
#define GPIO_INPUT_PIN           (1ULL << GPIO_PIN_KEY)
#define KEY_SET_UP               1
#define KEY_SET_DOWN             0
#endif

#define FAST_REBOOT_NUM          3

#define HEAPSTACK_MONITOR        0

#endif
