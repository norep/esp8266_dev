#ifndef __USERMAIN_H_
#define __USERMAIN_H_

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "esp_system.h"
#include "esp_log.h"
#include "esp_err.h"
#include "esp_ota_ops.h"

#include "esp_spi_flash.h"
#include "driver/gpio.h"

#include "my_common.h"
#include "my_wifi.h"
#include "my_http.h"

#define KEY_SHORT_DOWN      BIT0
#define KEY_LONG_DOWN       BIT1

#define MAX_HTTP_OUTPUT_BUFFER 2048

extern uint32_t esp_get_time(void);

#endif
