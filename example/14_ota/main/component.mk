#
# "main" pseudo-component makefile.
#
# (Uses default behaviour of compiling all source files in directory, adding 'include' to include path.)

COMPONENT_SRCDIRS += ../../../peripheral_driver/my_wifi
COMPONENT_SRCDIRS += ../../../peripheral_driver/my_http
COMPONENT_SRCDIRS += ../../../peripheral_driver/my_common
# //这用于添加，需要编译的头文件
COMPONENT_ADD_INCLUDEDIRS += ../../../peripheral_driver/my_wifi
COMPONENT_ADD_INCLUDEDIRS += ../../../peripheral_driver/my_http
COMPONENT_ADD_INCLUDEDIRS += ../../../peripheral_driver/my_common
