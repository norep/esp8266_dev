#
# "main" pseudo-component makefile.
#
# (Uses default behaviour of compiling all source files in directory, adding 'include' to include path.)

COMPONENT_SRCDIRS += ../../../peripheral_driver/my_wifi
COMPONENT_SRCDIRS += ../../../peripheral_driver/my_common
COMPONENT_SRCDIRS += ../../../peripheral_driver/my_http
COMPONENT_SRCDIRS += ../../../hardware_driver_library/DS18B20
COMPONENT_SRCDIRS += ../../../hardware_driver_library/Aht20
# //这用于添加，需要编译的头文件

COMPONENT_ADD_INCLUDEDIRS += ./
COMPONENT_ADD_INCLUDEDIRS += ../../../peripheral_driver/my_wifi
COMPONENT_ADD_INCLUDEDIRS += ../../../peripheral_driver/my_common
COMPONENT_ADD_INCLUDEDIRS += ../../../peripheral_driver/my_http
COMPONENT_ADD_INCLUDEDIRS += ../../../hardware_driver_library/DS18B20
COMPONENT_ADD_INCLUDEDIRS += ../../../hardware_driver_library/Aht20

COMPONENT_EMBED_TXTFILES := ./index.html
COMPONENT_EMBED_TXTFILES += ./save.html
