
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "esp_system.h"
#include "esp_log.h"
#include "esp_err.h"

#include "esp_spi_flash.h"
#include "driver/gpio.h"
#include "driver/uart.h"
#include "driver/spi.h"

#include "my_uart.h"
#include "my_spi.h"

static const char *TAG = "spi demo";

#define GPIO_PIN_LED 2
#define GPIO_OUTPUT_PIN (1UL << GPIO_PIN_LED)

void Gpio_Init(void)
{
    gpio_config_t IO_config;

    IO_config.pin_bit_mask = GPIO_OUTPUT_PIN;
    IO_config.mode = GPIO_MODE_OUTPUT;
    IO_config.pull_up_en = GPIO_PULLUP_DISABLE;
    IO_config.pull_down_en = GPIO_PULLDOWN_DISABLE;
    IO_config.intr_type = GPIO_INTR_DISABLE;

    gpio_config(&IO_config);
}

void get_system_info(void)
{
    ESP_LOGI(TAG,"\nhello world\n");
    //获取IDF版本
    ESP_LOGI(TAG,"SDK version:%s\n", esp_get_idf_version());

    //获取芯片可用内存
    ESP_LOGI(TAG,"esp_get_free_heap_size : %d  \n", esp_get_free_heap_size());
    //获取从未使用过的最小内存
    ESP_LOGI(TAG,"esp_get_minimum_free_heap_size : %d  \n", esp_get_minimum_free_heap_size());
    //获取芯片的内存分布，返回值具体见结构体 flash_size_map
    ESP_LOGI(TAG,"system_get_flash_size_map(): %d \n", system_get_flash_size_map());
    //获取mac地址（station模式）
    uint8_t mac[6];
    esp_read_mac(mac, ESP_MAC_WIFI_STA);
    ESP_LOGI(TAG,"esp_read_mac(): %02x:%02x:%02x:%02x:%02x:%02x \n", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);

    /* Print chip information */
    esp_chip_info_t chip_info;
    esp_chip_info(&chip_info);
    ESP_LOGI(TAG,"This is ESP8266 chip with %d CPU cores, WiFi, ",
        chip_info.cores);
    ESP_LOGI(TAG,"silicon revision %d, ", chip_info.revision);
    ESP_LOGI(TAG,"%dMB %s flash\n", spi_flash_get_chip_size() / (1024 * 1024),
        (chip_info.features & CHIP_FEATURE_EMB_FLASH) ? "embedded" : "external");
}

void uart_recv_task(void *argv)
{
    uint8_t *buf = calloc(1024,1);
    int len = 0;
    while (1)
    {
        my_uart_recv_data(UART_NUM_0,buf,&len);
        if(len > 0){
            buf[len] = 0;
            ESP_LOGW(TAG,"RX:%s",buf);
        }
    }
}

void uart_send_task(void *argv)
{
    while(1) {
        my_uart_send_data(UART_NUM_0,(uint8_t *)"hello world\r\n",strlen("hello world\r\n"));
        vTaskDelay(500/ portTICK_RATE_MS);
    }
}

void spi_send_task(void *argv)
{
    uint8_t buf[4] ={0};
    buf[0] = 0x12;
    buf[1] = 0x34;
    buf[2] = 0x56;
    buf[3] = 0x78;

    while(1) {
        my_spi_write(buf, sizeof(buf));
        vTaskDelay(1000/ portTICK_RATE_MS);
    }
}

void app_main()
{   
    ESP_LOGE(TAG,"ESP_LOGE");
    ESP_LOGW(TAG,"ESP_LOGW");
    ESP_LOGI(TAG,"ESP_LOGI");
    ESP_LOGD(TAG,"ESP_LOGD");
    ESP_LOGV(TAG,"ESP_LOGV");

    ESP_LOGI(TAG,"start Gpio_Init");
    Gpio_Init();
    get_system_info();
    
    hl_uart_config_t uart_config;
    uart_config.uart_num = UART_NUM_0;
    uart_config.bate = 74880;

    ESP_LOGI(TAG,"start my_usart_init");
    my_uart_init(uart_config);
    ESP_LOGI(TAG,"create uart_recv_task");
    xTaskCreate(uart_recv_task,"uart_recv_task",1024*2,NULL, 20, NULL);

    my_spi_init(NULL);

    ESP_LOGI(TAG,"create spi_send_task");
    xTaskCreate(spi_send_task,"spi_send_task",1024*2,NULL, 21, NULL);

    ESP_LOGI(TAG,"start run");
}
