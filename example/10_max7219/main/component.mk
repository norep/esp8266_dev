#
# "main" pseudo-component makefile.
#
# (Uses default behaviour of compiling all source files in directory, adding 'include' to include path.)

COMPONENT_SRCDIRS += ../../../peripheral_driver/my_uart
COMPONENT_SRCDIRS += ../../../peripheral_driver/my_spi
COMPONENT_SRCDIRS += ../../../hardware_driver_library/Max7219
# //这用于添加，需要编译的头文件
COMPONENT_ADD_INCLUDEDIRS += ../../../peripheral_driver/my_uart
COMPONENT_ADD_INCLUDEDIRS += ../../../peripheral_driver/my_spi
COMPONENT_ADD_INCLUDEDIRS += ../../../hardware_driver_library/Max7219