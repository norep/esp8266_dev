#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "esp_system.h"
#include "esp_log.h"
#include "esp_err.h"

#include "esp_spi_flash.h"
#include "driver/gpio.h"

static const char *TAG = "hello world";
#define LOG_LOCAL_LEVEL  ESP_LOG_DEBUG

#define GPIO_PIN_LED 2
#define GPIO_OUTPUT_PIN (1UL << GPIO_PIN_LED)

void Gpio_Init(void)
{
    gpio_config_t IO_config;

    IO_config.pin_bit_mask = GPIO_OUTPUT_PIN;
    IO_config.mode = GPIO_MODE_OUTPUT;
    IO_config.pull_up_en = GPIO_PULLUP_DISABLE;
    IO_config.pull_down_en = GPIO_PULLDOWN_DISABLE;
    IO_config.intr_type = GPIO_INTR_DISABLE;

    gpio_config(&IO_config);
}

void app_main()
{   
    ESP_LOGE(TAG,"ESP_LOGE");
    ESP_LOGW(TAG,"ESP_LOGW");
    ESP_LOGI(TAG,"ESP_LOGI");
    ESP_LOGD(TAG,"ESP_LOGD");
    ESP_LOGV(TAG,"ESP_LOGV");

    ESP_LOGI(TAG,"start Gpio_Init");
    Gpio_Init();
    while(1){
        printf("\nhello world\n");
        //获取IDF版本
        printf("SDK version:%s\n", esp_get_idf_version());

        //获取芯片可用内存
        printf("esp_get_free_heap_size : %d  \n", esp_get_free_heap_size());
        //获取从未使用过的最小内存
        printf("esp_get_minimum_free_heap_size : %d  \n", esp_get_minimum_free_heap_size());
        //获取芯片的内存分布，返回值具体见结构体 flash_size_map
        printf("system_get_flash_size_map(): %d \n", system_get_flash_size_map());
        //获取mac地址（station模式）
        uint8_t mac[6];
        esp_read_mac(mac, ESP_MAC_WIFI_STA);
        printf("esp_read_mac(): %02x:%02x:%02x:%02x:%02x:%02x \n", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);

        /* Print chip information */
        esp_chip_info_t chip_info;
        esp_chip_info(&chip_info);
        printf("This is ESP8266 chip with %d CPU cores, WiFi, ",
            chip_info.cores);
        printf("silicon revision %d, ", chip_info.revision);
        printf("%dMB %s flash\n", spi_flash_get_chip_size() / (1024 * 1024),
            (chip_info.features & CHIP_FEATURE_EMB_FLASH) ? "embedded" : "external");

        gpio_set_level(GPIO_PIN_LED,1);
        vTaskDelay(1000 / portTICK_PERIOD_MS);
        gpio_set_level(GPIO_PIN_LED,0);
        vTaskDelay(1000 / portTICK_PERIOD_MS);
        gpio_set_level(GPIO_PIN_LED,1);
        vTaskDelay(1000 / portTICK_PERIOD_MS);
        gpio_set_level(GPIO_PIN_LED,0);
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
    printf("Restarting now.\n");
    fflush(stdout);
    esp_restart();
}
