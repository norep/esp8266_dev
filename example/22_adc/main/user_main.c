#include "user_main.h"

static const char *TAG = "global";

#if SUPPORT_FIRMWARE_OTA
static const char *ota_version_url = "http://norep.gitee.io/ota/ota_test/version";
static const char *ota_image_url = "http://norep.gitee.io/ota/ota_test/ota.bin";
#endif

static int user_get_cb(httpd_req_t *req)
{
    httpd_resp_send(req, index_html_start, index_html_end - index_html_start);
    return 0;
}

static int user_post_cb(httpd_req_t *req, char *content, int len)
{
    system_status_t *sys_sta = get_sys_sta();
    char ssid[32];
    char passwd[32];
    int ret = httpd_query_key_value(content,"ssid",ssid,sizeof(ssid));
    if(ret == ESP_OK) {
        printf("ssid = %s\n",ssid);
    }
    else {
        printf("error = %d\n",ret);
    }

    ret = httpd_query_key_value(content,"password",passwd,sizeof(passwd));
    if(ret == ESP_OK) {
        printf("password = %s\n",passwd);
    }
    else {
        printf("error = %d\n",ret);
    }

    if (strlen(ssid) && strlen(passwd)) {
        my_nvs_wifi_info_set(ssid, passwd);
    }
    httpd_resp_send(req, save_html_start, save_html_end - save_html_start);
    sys_sta->reboot_flag = 1;
    return 0;
}

static int shortdown(void)
{
    ESP_LOGW(TAG, "===== SHOART DOWN =====");
    return 0;
}
    
static int longdown(void)
{
    ESP_LOGW(TAG, "===== LONG DOWN =====");
    return 0;
}

static void WIFI_Connect_cb(char *ip)
{
    system_status_t *sys_sta = get_sys_sta();
    sys_sta->network_sta = 1;
    ESP_LOGW(TAG, "wifi connect : %s", ip);
}

static void WIFI_Disconnect_cb(void)
{
    system_status_t *sys_sta = get_sys_sta();
    sys_sta->network_sta = 0;
    ESP_LOGW(TAG, "wifi disconnect");
}

static void WIFI_AP_STAConnect_cb(void)
{
    ESP_LOGW(TAG, "WIFI_AP_STAConnect_cb");

    my_http_cb_t user_cb;
    user_cb.get_cb = user_get_cb;
    user_cb.post_cb = user_post_cb;
    my_http_server_set_callback(user_cb);
    ESP_LOGW(TAG, "Starting webserver");
    my_start_webserver();

}

static void WIFI_AP_STADisconnect_cb(void)
{
    ESP_LOGW(TAG, "WIFI_AP_STADisconnect_cb");
}

static void Networl_init(void)
{
    my_wifi_t wifi_info = {};
    wifi_mode_t wifi_init_mode = WIFI_MODE_AP;

    if (my_nvs_wifi_info_get(&wifi_info)) {

        memset(&wifi_info, 0, sizeof(wifi_info));
        memcpy(wifi_info.ssid, DEFAULT_AP_MODE_SSID, strlen(DEFAULT_AP_MODE_SSID));

        if (strlen(DEFAULT_AP_MODE_PASSWORD) > 0) {
            memmove(wifi_info.password, DEFAULT_AP_MODE_PASSWORD, strlen(DEFAULT_AP_MODE_PASSWORD));
        }

        wifi_info.max_connection = 1;

        wifi_info.ap_staconnect_cb = WIFI_AP_STAConnect_cb;
        wifi_info.ap_stadisconnect_cb = WIFI_AP_STADisconnect_cb;

        wifi_init_mode = WIFI_MODE_AP;
    } else {
        wifi_info.connect_cb = WIFI_Connect_cb;
        wifi_info.disconnect_cb = WIFI_Disconnect_cb;

        wifi_init_mode = WIFI_MODE_STA;
    }
    my_wifi_init(&wifi_info, wifi_init_mode);
}



static int Get_avg(uint16_t *data, int len)
{
    int i = 0;
    uint64_t sum = 0;
    for (i = 0; i < len; i++) {
        sum += data[i];
    }
    return (int)((double)sum / 100);
}

/**
 * @brief 获取adc对应的电压
 * @param adc_cur : adc采集的当前值
 * @param adc_scale : 外部adc电阻分压倍率
 * @return ：计算出的电压
 * */
static float Get_voltage(uint16_t adc_cur, int adc_scale)
{
    double voltage = 0;

    // voltage = (adc_cur / 1023.0) * adc_scale;
	voltage = (adc_cur - 12) / 198.0;
    return (float)voltage;
}

// Depend on menuconfig->Component config->PHY->vdd33_const value
// When measuring system voltage(ADC_READ_VDD_MODE), vdd33_const must be set to 255.
static void user_adc_init(void)
{
    uint16_t adc_data[100];

    adc_config_t adc_config;
    adc_config.mode = ADC_READ_TOUT_MODE;
    adc_config.clk_div = 8; // ADC sample collection clock = 80MHz/clk_div = 10MHz
    ESP_ERROR_CHECK(adc_init(&adc_config));

    uint16_t adc_val = 0;
    float adc_vol = 0;

    while(1) {

/*************单次测量************/
        // adc_read(&adc_data[0]);
        // adc_val = adc_data[0];
/*******************************/

/*************多次测量************/
        if (ESP_OK != adc_read_fast(adc_data, 100)) {
            ESP_LOGE(TAG, "adc read fast err\r\n");
        }
        adc_val = Get_avg(adc_data, 100);
/*******************************/
        adc_vol = Get_voltage(adc_val, 1);
        ESP_LOGI(TAG, "adc read: %d, voltage:%.2f\n", adc_val, adc_vol);
        vTaskDelay(100 / portTICK_RATE_MS);
    };
}

void app_main()
{
    system_status_t *sys_sta = get_sys_sta();
    const esp_app_desc_t *app_desc = esp_ota_get_app_description();
    sys_sta->version = (char *)app_desc->version;
    ESP_LOGI(TAG, "app_version:%s", sys_sta->version); 

#if SUPPORT_FIRMWARE_OTA
    sys_sta->ota_version_path = ota_version_url;
    sys_sta->ota_image_path = ota_image_url;
#endif

    system_init();

    printf("free_heap_size: %d KB\n", esp_get_free_heap_size() / 1024);
    printf("minimum_free_heap_size: %d KB\n", esp_get_minimum_free_heap_size() / 1024);

    set_key_shortDown_callback(200, shortdown);
    set_key_longDown_callback(2000, longdown);

    ESP_LOGI(TAG, "start connect wifi...");
    Networl_init();
    ESP_LOGI(TAG, "Run");

    user_adc_init();
}
