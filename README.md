# esp8266_dev

# 安装必备软件
## Ubuntu: 
    sudo apt-get install gcc git wget make libncurses-dev flex bison gperf python python-serial

# 安装交叉编译工具链
1. 下载工具链 wget https://dl.espressif.com/dl/xtensa-lx106-elf-gcc8_4_0-esp-2020r3-linux-amd64.tar.gz
2. 解压 tar -xzvf xtensa-lx106-elf-linux64-1.22.0-100-ge567ec7-5.2.0.tar.gz

# 安装ESP8266_RTOS_SDK 版本:v3.4
## 拉取方法
1. 通过子模块拉取：git submodule update --init --recursive
2. 直接拉取：git clone -b release/v5.0 --recursive https://github.com/espressif/esp-idf.git

# 设置环境变量
1. export IDF_PATH=$HOME/esp8266_dev/ESP8266_RTOS_SDK
2. export PATH=$PATH:$HOME/esp8266-gcc-tool/bin

# 添加串口权限
    sudo usermod -a -G dialout $USER

# 安装python模块 python版本3.8
    python -m pip install --user -r $IDF_PATH/requirements.txt
## pip加速方法
1. 创建配置文件
```bash
mkdir ~/.pip
cd ~/.pip
touch pip.conf
```
2. 在pip.conf中写入配置信息
```bash
[global]
timeout = 60
index-url = https://pypi.tuna.tsinghua.edu.cn/simple
trusted-host = pypi.douban.com
```
