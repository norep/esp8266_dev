#ifndef __USERMAIN_H_
#define __USERMAIN_H_

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "esp_system.h"
#include "esp_log.h"
#include "esp_err.h"
#include "esp_ota_ops.h"
#include "esp_spi_flash.h"
#include "driver/gpio.h"
#include "nvs_flash.h"
#include "nvs.h"


#include "driver/i2c.h"

#include "my_config.h"
#include "my_common.h"
#include "my_wifi.h"
#include "ds18b20.h"
#include "my_mqtt.h"

extern const char index_html_start[] asm("_binary_index_html_start");
extern const char index_html_end[]   asm("_binary_index_html_end");

extern const char save_html_start[] asm("_binary_save_html_start");
extern const char save_html_end[]   asm("_binary_save_html_end");

#define DS18B20_GPIO                GPIO_NUM_0
#define GPIO_DS18B20_MASK          (1ULL << DS18B20_GPIO)

#define LED_R_PIN                   GPIO_NUM_15
#define LED_G_PIN                   GPIO_NUM_12
#define LED_B_PIN                   GPIO_NUM_13
#define GPIO_RGB_MASK               ((1ULL << LED_R_PIN) | (1ULL << LED_G_PIN) | (1ULL << LED_B_PIN))

#define LED_R_SET_UP                1
#define LED_G_SET_UP                1
#define LED_B_SET_UP                1

#define LED_R_SET_DOWN              0
#define LED_G_SET_DOWN              0
#define LED_B_SET_DOWN              0

#endif
