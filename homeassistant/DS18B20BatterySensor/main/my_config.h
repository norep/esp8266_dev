#ifndef __MY_CONFIG_H__
#define __MY_CONFIG_H__

#define DEFAULT_AP_MODE_SSID     "esp8266_ap"
#define DEFAULT_AP_MODE_PASSWORD ""

#define MY_HOST_NAME             "outdoorTemperature"

#define ENABLE_LED_FUNCTIONS     1

#if ENABLE_LED_FUNCTIONS
#define GPIO_LED_PIN             GPIO_NUM_2
#define GPIO_LED_MASK            (1ULL << GPIO_LED_PIN)
#define LED_WORK_UP              0
#define LED_WORK_DOWN            1
#endif

#define ENABLE_KEY_FUNCTIONS     1

#if ENABLE_KEY_FUNCTIONS
#define GPIO_KEY_PIN             GPIO_NUM_0
#define GPIO_KEY_MASK            (1ULL << GPIO_KEY_PIN)
#define KEY_SET_UP               1
#define KEY_SET_DOWN             0
#endif

#define FAST_REBOOT_NUM          3

#define HEAPSTACK_MONITOR        0
#define SUPPORT_FIRMWARE_OTA     0

#endif
