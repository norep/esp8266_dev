#ifndef __USERMAIN_H_
#define __USERMAIN_H_

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "esp_system.h"
#include "esp_log.h"
#include "esp_err.h"
#include "esp_ota_ops.h"
#include "esp_spi_flash.h"
#include "driver/gpio.h"
#include "nvs_flash.h"
#include "nvs.h"

#include "my_config.h"
#include "my_common.h"
#include "my_wifi.h"

#include "my_spi.h"
#include "max7219.h"
#include "dht11.h"
#include "my_mqtt.h"

extern const char index_html_start[] asm("_binary_index_html_start");
extern const char index_html_end[]   asm("_binary_index_html_end");

extern const char save_html_start[] asm("_binary_save_html_start");
extern const char save_html_end[]   asm("_binary_save_html_end");

#define DHT11_GPIO                 GPIO_NUM_5
#define GPIO_DHT11_PIN_MASK        (1ULL << DHT11_GPIO)

#endif
