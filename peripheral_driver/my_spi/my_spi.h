#ifndef __MY_SPI_H
#define __MY_SPI_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "esp_spi_flash.h"
#include "driver/spi.h"
#include "esp_log.h"

// static void IRAM_ATTR spi_event_callback(int event, void *arg)
// {
// 	switch (event) {
// 		case SPI_INIT_EVENT: {
// 		}
// 		break;
// 		case SPI_TRANS_START_EVENT: {
// 		}
// 		break;
// 		case SPI_TRANS_DONE_EVENT: {
// 		}
// 		break;
// 		case SPI_DEINIT_EVENT: {
// 		}
// 		break;
// 	}
// }

void my_spi_init(spi_event_callback_t cb);
esp_err_t my_spi_write(uint8_t *txbuf, uint8_t txlen);
esp_err_t my_spi_read(uint8_t *rxbuf, uint8_t rxlen);

#endif
