#ifndef __MY_WIFI_H_
#define __MY_WIFI_H_

#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_log.h"
#include "esp_netif.h"
#include "esp_event.h"
#include "esp_wifi.h"
#include "nvs.h"
#include "nvs_flash.h"

#include "lwip/err.h"
#include "lwip/sys.h"

#include "my_config.h"

/***************************
 * 函数名:wifi连接成功回调函数
 * 功能：
 * 参数1：获取到的IP
 * 参数2：
 * 返回值：
 * 作者:
 * **************************/
typedef void (*WIFI_Connect_callback)(char *ip);

/***************************
 * 函数名:wifi连接失败回调函数
 * 功能：
 * 参数1：
 * 参数2：
 * 返回值：
 * 作者:
 * **************************/
typedef void (*aWIFI_Disconnect_callback)(void);

/***************************
 * 函数名:wifi AP模式下STA连接成功
 * 功能：
 * 参数1：STA设备IP
 * 参数2：
 * 返回值：
 * 作者:
 * **************************/
typedef void (*WIFI_AP_STAConnect_callback)(void);

/***************************
 * 函数名:wifi AP模式下STA断开连接
 * 功能：
 * 参数1：
 * 参数2：
 * 返回值：
 * 作者:
 * **************************/
typedef void (*WIFI_AP_STADisconnect_callback)(void);

#define EXAMPLE_ESP_MAXIMUM_RETRY  5

typedef struct my_wifi
{
	uint8_t ssid[32];     
	uint8_t password[32];
	uint8_t max_connection; // AP模式时允许几个STA连接
	WIFI_Connect_callback connect_cb;
	aWIFI_Disconnect_callback disconnect_cb;
	WIFI_AP_STAConnect_callback ap_staconnect_cb;
	WIFI_AP_STADisconnect_callback ap_stadisconnect_cb;
}my_wifi_t;

void my_wifi_init(my_wifi_t *info, wifi_mode_t mode);

#endif

