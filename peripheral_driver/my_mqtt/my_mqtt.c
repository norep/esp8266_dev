#include "my_mqtt.h"

static const char *TAG = "my_mqtt";

static esp_mqtt_client_handle_t g_client;

static my_mqtt_init_info_t g_init_info = {0};

static int mqtt_connect_flag = 0;

static esp_err_t my_mqtt_event_handler_cb(esp_mqtt_event_handle_t event)
{
    switch (event->event_id) {
        case MQTT_EVENT_CONNECTED:
            ESP_LOGI(TAG, "MQTT_EVENT_CONNECTED");
			mqtt_connect_flag = 1;
			if (g_init_info.connect_cb != NULL) {
				g_init_info.connect_cb();
			}
            break;
        case MQTT_EVENT_DISCONNECTED:
            ESP_LOGI(TAG, "MQTT_EVENT_DISCONNECTED");
			mqtt_connect_flag = 0;
			if (g_init_info.disconnect_cb != NULL) {
				g_init_info.disconnect_cb();
			}
            break;
        case MQTT_EVENT_SUBSCRIBED:
            ESP_LOGI(TAG, "MQTT_EVENT_SUBSCRIBED, msg_id=%d", event->msg_id);
            break;
        case MQTT_EVENT_UNSUBSCRIBED:
            ESP_LOGI(TAG, "MQTT_EVENT_UNSUBSCRIBED, msg_id=%d", event->msg_id);
            break;
        case MQTT_EVENT_PUBLISHED:
            ESP_LOGI(TAG, "MQTT_EVENT_PUBLISHED, msg_id=%d", event->msg_id);
            break;
        case MQTT_EVENT_DATA:
            ESP_LOGI(TAG, "MQTT_EVENT_DATA");
			if (g_init_info.received_cb != NULL) {
				g_init_info.received_cb(event->topic, event->topic_len, event->data, event->data_len);
			}
            break;
        case MQTT_EVENT_ERROR:
            ESP_LOGI(TAG, "MQTT_EVENT_ERROR");
            break;
        default:
            ESP_LOGI(TAG, "Other event id:%d", event->event_id);
            break;
    }
    return ESP_OK;
}

static void my_mqtt_event_handler(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data) {
    ESP_LOGD(TAG, "Event dispatched from event loop base=%s, event_id=%d", base, event_id);
    my_mqtt_event_handler_cb(event_data);
}

int my_mqtt_init(my_mqtt_init_info_t init_cfg)
{
	memset(&g_init_info, 0, sizeof(g_init_info));
	memmove(&g_init_info, &init_cfg, sizeof(g_init_info));

    esp_mqtt_client_config_t mqtt_cfg = {0};
	mqtt_cfg.host = g_init_info.host;
	mqtt_cfg.port = g_init_info.port;
	mqtt_cfg.username = g_init_info.username;
	mqtt_cfg.password = g_init_info.password;

    ESP_LOGW(TAG,"esp_mqtt_client_init");
    g_client = esp_mqtt_client_init(&mqtt_cfg);
    ESP_LOGW(TAG,"esp_mqtt_client_register_event");
    esp_mqtt_client_register_event(g_client, ESP_EVENT_ANY_ID, my_mqtt_event_handler, g_client);
    ESP_LOGW(TAG,"esp_mqtt_client_start");
    esp_mqtt_client_start(g_client);
	return 0;
}

int my_mqtt_subscribe(const char *topic, int qos)
{
    int msg_id;
	if (mqtt_connect_flag) {
		msg_id = esp_mqtt_client_subscribe(g_client, topic, qos);
		if (msg_id == -1) {
			return -1;
		}
		ESP_LOGI(TAG, "sent subscribe successful, msg_id=%d", msg_id);
		return 0;
	} else {
		return -1;
	}
}

int my_mqtt_unsubscribe(const char *topic)
{
    int msg_id;
	if (mqtt_connect_flag) {
		msg_id = esp_mqtt_client_unsubscribe(g_client, topic);
		if (msg_id == -1) {
			return -1;
		}
		ESP_LOGI(TAG, "sent unsubscribe successful, msg_id=%d", msg_id);
		return 0;
	} else {
		return -1;
	}
}

int my_mqtt_publish(const char *topic, const char *data, int len, int qos, int retain)
{
    int msg_id;
	if (mqtt_connect_flag) {

		msg_id = esp_mqtt_client_publish(g_client, topic, data, len, qos, retain);
		if (msg_id == -1) {
			ESP_LOGE(TAG, "sent publish fail, msg_id=%d", msg_id);
			return -1;
		}
		ESP_LOGI(TAG, "sent publish successful, msg_id=%d", msg_id);
		return 0;
	} else {
		return -1;
	}
}



