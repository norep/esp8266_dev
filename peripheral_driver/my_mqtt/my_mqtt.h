#ifndef __MY_MQTT_H__
#define __MY_MQTT_H__

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "esp_system.h"
#include "esp_log.h"
#include "esp_err.h"
#include "esp_ota_ops.h"

#include "esp_event.h"
#include "mqtt_client.h"


typedef void (*MY_MQTT_Connect_callback)(void);
typedef void (*MY_MQTT_DISConnect_callback)(void);
typedef void (*MY_MQTT_RecData_callback)(char *topic, int topic_len, char *data, int data_len);


typedef struct my_mqtt_init_info {
	const char *host;
	uint32_t port;
	const char *username;                   /*!< MQTT username */
    const char *password;                   /*!< MQTT password */
	MY_MQTT_Connect_callback connect_cb;
	MY_MQTT_DISConnect_callback disconnect_cb;
	MY_MQTT_RecData_callback received_cb;
} my_mqtt_init_info_t;

int my_mqtt_init(my_mqtt_init_info_t init_cfg);

int my_mqtt_subscribe(const char *topic, int qos);

int my_mqtt_unsubscribe(const char *topic);

/*
 * @param topic     topic string
 * @param data      payload string (set to NULL, sending empty payload message)
 * @param len       data length, if set to 0, length is calculated from payload string
 * @param qos       qos of publish message
 * @param retain    retain flag\
 */
int my_mqtt_publish(const char *topic, const char *data, int len, int qos, int retain);

#endif
