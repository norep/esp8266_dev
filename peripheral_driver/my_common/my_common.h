#ifndef __MY_COMMON_H_
#define __MY_COMMON_H_

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "esp_system.h"
#include "esp_log.h"
#include "esp_err.h"
#include "esp_ota_ops.h"

#include "esp_spi_flash.h"
#include "driver/gpio.h"
#include "lwip/apps/sntp.h"

#include "my_config.h"
#include "my_wifi.h"
#include "my_http.h"

extern uint32_t esp_get_time(void);
#define MAX_HTTP_OUTPUT_BUFFER 2048	

#define NVS_SYSTEM_DATA_SPACE                "storage"
#define POWER_ON_COUNT_KEY                   "power_on_num"
#define FAST_REBOOT_NUM_KEY                  "fast_up_num"
#define WIFI_SSID_KEY                        "wifi_ssid"
#define WIFI_PASSWD_KEY                      "wifi_passwd"

typedef int(*keyDownCallback)(void);
typedef int(*system_monitor_callback)(void);

typedef struct {
	uint8_t LED_OPEN:1;
	uint8_t LED_SLOW_BLINK:1;
	uint8_t LED_FAST_BLINK:1;
} LED_MODE_s;

typedef struct gpio_key
{
	int dowmFlag;
	uint32_t dowmStartTime;
	int shortdownIsWork;
	int longdownIsWork;
	int shortdown_ms;
	int longdown_ms;
	keyDownCallback shortdowncb;
	keyDownCallback londdowncb;
}gpio_key_s;

typedef struct system_status
{
	uint8_t led_sta;
	uint8_t network_sta;
	uint8_t ota_sta;
	uint8_t reboot_flag;
	LED_MODE_s led_mode;
	gpio_key_s key_info;
	wifi_mode_t wifi_mode;
	const char *ota_version_path;
	const char *ota_image_path;
	char *version;
	char ipaddr[20];
	system_monitor_callback sys_cb;
} system_status_t;

typedef struct task_handle_managa {
	char name[32];
	TaskHandle_t handle;
	struct task_handle_managa *next;
} task_handle_managa_t;

typedef enum {
	MY_NVS_I32_TYPE,
	MY_NVS_I64_TYPE,
	MY_NVS_STR_TYPE,
	MY_NVS_BLOB_TYPE,
} my_nvs_data_type_e;

extern struct tm timeinfo;

int set_key_shortDown_callback(int worktime, keyDownCallback fn);
int set_key_longDown_callback(int worktime, keyDownCallback fn);

system_status_t *get_sys_sta(void);
void system_init(void);

void my_user_nvs_init(void);
void my_nvs_info_erase(void);
int my_nvs_data_rw(const char *key, void *buf, size_t *len, my_nvs_data_type_e type, nvs_open_mode mode);
int my_nvs_wifi_info_get(my_wifi_t *wifi_info);
int my_nvs_wifi_info_set(char *ssid, char *passwd);

void new_task(TaskFunction_t pxTaskCode, const char * const pcName, 
						const configSTACK_DEPTH_TYPE usStackDepth,
						void * const pvParameters,UBaseType_t uxPriority);
void delete_task(const char * const pcName);
#endif
