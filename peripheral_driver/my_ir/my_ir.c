#include "my_ir.h"

static const char *TAG = "my_ir";

void my_ir_rx_init(ir_rx_config_t *ir_rx_config)
{
    ir_rx_init(ir_rx_config);
}

void my_ir_read_data_once(ir_rx_nec_data_t *ir_data)
{
	ir_rx_recv_data(ir_data, 1, portMAX_DELAY);
	ESP_LOGI(TAG, "addr1: 0x%x, addr2: 0x%x, cmd1: 0x%x, cmd2: 0x%x", ir_data->addr1, ir_data->addr2, ir_data->cmd1, ir_data->cmd2);
}
