#ifndef __MY_IR__
#define __MY_IR__

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "esp_log.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "esp_system.h"

#include "driver/ir_rx.h"


#include "user_main.h"

void my_ir_rx_init(ir_rx_config_t *ir_rx_config);
void my_ir_read_data_once(ir_rx_nec_data_t *ir_data);

#endif