#ifndef __MY_HTTP_H_
#define __MY_HTTP_H_
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "esp_system.h"
#include "esp_log.h"
#include "esp_err.h"
#include "esp_ota_ops.h"

#include "esp_http_client.h"
#include "esp_tls.h"
#include "esp_https_ota.h"
#include <esp_http_server.h>

#include <sys/param.h>

#include "my_common.h"

typedef int (*http_server_get_callback)(httpd_req_t *req);
typedef int (*http_server_post_callback)(httpd_req_t *req, char *content, int len);

typedef struct my_http_cb
{
	http_server_get_callback get_cb;
	http_server_post_callback post_cb;
} my_http_cb_t;

int my_http_server_set_callback(my_http_cb_t user_cb);
httpd_handle_t my_start_webserver(void);
void my_stop_webserver(httpd_handle_t server);

int start_ota(const char *url);
int http_client_GET(esp_http_client_config_t *config);
int http_client_GET_test(void);

#endif

